package org.spiderflow.core.executor.function;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.spiderflow.annotation.Comment;
import org.spiderflow.annotation.Example;
import org.spiderflow.core.config.InfoInvProperties;
import org.spiderflow.executor.FunctionExecutor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Map;


/**
 * @Description 输出到信息侦查
 * @Author 万飞
 * @Date 2022/06/29 11:45
 */
@Component
@Comment("InfoInv常用方法")
@Slf4j
public class InfoInvFunctionExecutor implements FunctionExecutor{
	public static RestTemplate restTemplate;
	public static InfoInvProperties infoInvProperties;

	@Override
	public String getFunctionPrefix() {
		return "infoInv";
	}

	@Comment("把map存入infoInv")
	@Example("${infoInv.insert({'name':'jack','age',18})")
	public static void insert(Map<String, Object> map) throws IOException {
		if (map == null || map.isEmpty()) {
			return;
		}

		log.info("存入infoInv map: {}", JSONUtil.toJsonStr(map));

		// 输出到信息侦查接口
		JSONObject result = saveInfoInv(map);
		log.info("保存到信息侦查服务, 结果: {}", JSONUtil.toJsonStr(result));
	}

	/**
	 * 保存到信息侦查接口
	 * @param map
	 * @return
	 */
	private static JSONObject saveInfoInv(Map<String, Object> map) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", infoInvProperties.getApiToken());
		HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
		ResponseEntity<JSONObject> response = restTemplate.postForEntity(StrUtil.format("{}/from-data/mapSave", infoInvProperties.getUrlPrefix()), entity, JSONObject.class);
		if (response.getStatusCode() != HttpStatus.OK) {
			log.error("调用信息侦查接口存储错误，错误原因: {}", response.getBody());
			Assert.isFalse(true, "调用信息侦查接口存储失败");
		}
		return response.getBody();
	}
		
}
