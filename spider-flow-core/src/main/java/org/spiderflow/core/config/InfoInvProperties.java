package org.spiderflow.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @Description 信息侦查属性
 * @Author 万飞
 * @Date 2021/4/15 14:44
 */
@Data
@Component
@ConfigurationProperties(prefix = "info-inv")
public class InfoInvProperties {

    /**
     * api token
     */
    private String apiToken;

    /**
     * url前缀
     */
    private String urlPrefix;
}
