package org.spiderflow.core.config;

import lombok.RequiredArgsConstructor;
import org.spiderflow.core.executor.function.InfoInvFunctionExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

/**
 * @Description InfoInvFunctionExecutor初始化别的类
 * @Author 万飞
 * @Date 2021/4/15 14:44
 */
@RequiredArgsConstructor
@Component
public class InfoInvInitConfiguration {
    private final RestTemplate restTemplate;
    private final InfoInvProperties infoInvProperties;

    @PostConstruct
    public void init() {
        InfoInvFunctionExecutor.infoInvProperties = infoInvProperties;
        InfoInvFunctionExecutor.restTemplate = restTemplate;
    }
}
