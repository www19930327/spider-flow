package org.spiderflow.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * @Description RestTemplateConfig
 * @Author WanFei
 * @Date 2021/4/28 11:08
 **/
@Component
public class RestTemplateConfig {

    /**
     * restTemplate
     * @return
     */
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        // 解决RestTemplate状态码为401时，获取不到body信息，需要依赖 httpclient
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        return  restTemplate;
    }

}